#ifndef INTERVIEW_CPP_DUMMY_H
#define INTERVIEW_CPP_DUMMY_H

#include <string>

namespace TwistersFury::Interview {
    class Dummy {
    public:
        static std::string getSomething();
    };

} // Interview

#endif //INTERVIEW_CPP_DUMMY_H
