#define CATCH_CONFIG_MAIN
#include "../../includes/catch.hpp"
#include "../../twistersfury/interview/Dummy.hpp"

using TwistersFury::Interview::Dummy;

TEST_CASE("Dummy") {
    SECTION("It Does Something") {
        REQUIRE(Dummy::getSomething() == "Something");
    }
}